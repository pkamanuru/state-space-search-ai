import pegSolitaireUtils
import config
import Queue
import heuristics

def ItrDeepSearch(pegSolitaireObject):
    """ Iterative deepening : prunes out the symmetric gamestates from further traversing and searching"""

    # Start gameState 
    start_state=pegSolitaireObject.gameState
    game_depth=1
    previous_level_nodes_expanded=0

    # This loop is for iteratively increasing the depth and making DFS
    while True:
        fringe_list=[]
        fringe_list.append((start_state,[],0))
        is_game_state_visited=dict()

        # This loop makes DFS using stack (abstrct, actual impplementation is using python list)
        while len(fringe_list) != 0:
            # Fringe consist of (gameState, trace_till_that_game_state, depth_of_that_gameState)
            (X,trace,game_state_depth)=fringe_list[len(fringe_list)-1]
            pegSolitaireObject.set_game_state(X)
            fringe_list.pop()

            # Checking for goal state
            if ( pegSolitaireObject.is_goalState() ):
                pegSolitaireObject.trace = list(trace)
                del fringe_list[:]
                break
            
            # Check if the depth reached is permitted depth according to the current deepening level.
            if game_state_depth < game_depth:
                game_state_depth+=1

                # Extract possible child states of current gamestate 
                valid_game_moves = pegSolitaireObject.get_all_possible_game_moves()
                saved_state=[row[:] for row in pegSolitaireObject.get_game_state()]
                
                # For all possible children states of current state
                for move in valid_game_moves:
                    (old_pos,direction) = move
                    pegSolitaireObject.set_game_state(saved_state) 
                    next_state=[row[:] for row in pegSolitaireObject.getNextState(old_pos,direction)]
                    next_state_string = pegSolitaireObject.get_gameState_string()
                    # Check if the child state is not yet visited
                    if next_state_string not in is_game_state_visited:
                        is_game_state_visited[next_state_string] = True

                        #Get all symmetrical gameState string and mark them visited
                        for each_game_state in set(heuristics.get_symmetric_game_states(next_state)):
                            is_game_state_visited[each_game_state] = True

                        y = list(trace)
                        new_pos=pegSolitaireObject.getNextPosition(old_pos,direction)
                        y.append(old_pos)
                        y.append(new_pos)
                        fringe_list.append((next_state,y,game_state_depth))

        # Check for the goal state, if not check for : if all nodes exhaustively checked in the last depth itself else continue for +1 depth
        if ( pegSolitaireObject.is_goalState() ):
            break
        else:
            if ( pegSolitaireObject.current_level_nodes_expanded > previous_level_nodes_expanded ):
                previous_level_nodes_expanded = pegSolitaireObject.current_level_nodes_expanded 
                pegSolitaireObject.current_level_nodes_expanded=0
                game_depth+=1
            else:
                break
    
    # Checking for game state being unsolvable 
    if len(pegSolitaireObject.trace) == 0:
        pegSolitaireObject.trace = ['Goal Not Found']
    return True

def aStarOne(pegSolitaireObject):
    """ Use Manhattan distance cost as heuristic. Prunes out symmetric Gamestates as well.
    Graph traversal cost g(n) is not included as the scale of h(n) and g(n) are not normalized 
    and thus using just h(n) gives better results. """

    return A_star_with_mentioned_heuristic(pegSolitaireObject,1)

def aStarTwo(pegSolitaireObject):
    """ Use Pagoda function with corner bias and merson region segement cost as heuristic. 
    Prunes out symmetric Gamestates as well.
    Graph traversal cost g(n) is not included as the scale of h(n) and g(n) are not normalized 
    and thus using just h(n) gives better results. """    

    return A_star_with_mentioned_heuristic(pegSolitaireObject,2)


def A_star_with_mentioned_heuristic(pegSolitaireObject, heuristic_number):
    """ Actual A* Code where heuristic function is called on the basis of the function parameter """

    # Start State
    start_state = pegSolitaireObject.gameState
    
    # Using priority Queue: Key of priority queue (heuristic cost: h(n)) is implitcitly defined
    # Priority Queue contains: ( h(n), g(n), gameState, trace_till_this_gameSate )
    priority_queue = Queue.PriorityQueue()
    priority_queue.put((get_heuristic_cost(start_state, heuristic_number), 0, start_state, []))
    is_game_state_visited = dict()

    # While priority Q is not empty traverse the game tree
    while not priority_queue.empty():
        (h_cost,g_cost,X,trace)= priority_queue.get()
        pegSolitaireObject.set_game_state(X)

        # Check for goal State
        if ( pegSolitaireObject.is_goalState() ):
            pegSolitaireObject.trace = list(trace)
            break
        
        # Get all possible children of current gameState
        valid_game_moves = pegSolitaireObject.get_all_possible_game_moves()
        saved_state=[row[:] for row in pegSolitaireObject.get_game_state()]
        
        # For all possible child gameStates, put in prio Q and traverse
        for move in valid_game_moves:
            (old_pos,direction) = move
            pegSolitaireObject.set_game_state(saved_state) 
            next_state = [row[:] for row in pegSolitaireObject.getNextState(old_pos,direction)]
            next_state_string = pegSolitaireObject.get_gameState_string()

            # If the popped out gameState if not visited yet, expand it well and traverse further
            if next_state_string not in is_game_state_visited:
                is_game_state_visited[next_state_string] = True

                # Mark all the symmetric gameStates as visited as well
                for each_game_state in set(heuristics.get_symmetric_game_states(next_state)):
                    is_game_state_visited[each_game_state] = True

                y = list(trace)
                new_pos = pegSolitaireObject.getNextPosition(old_pos,direction)
                y.append(old_pos)
                y.append(new_pos)

                # Use the below line to include the g(n) cost, but it is costing more, thus eliminated it from f(n)
                # priority_queue.put((g_cost + heuristics.weightedCost(next_state), g_cost+1, next_state,y))
                priority_queue.put((get_heuristic_cost(next_state, heuristic_number), g_cost+1, next_state,y))

    if len(pegSolitaireObject.trace) == 0:
        pegSolitaireObject.trace = ['Goal Not Found']
    return True

def get_heuristic_cost(gameState, heuristic_number):
    """ This is just to chose the heuristic function on the basis of function parameter 
    and thus help in reducing code duplicacy """
    
    if heuristic_number == 1:
        return heuristics.get_manhattan_distance_cost(gameState)
    elif heuristic_number == 2:
        return heuristics.pagoda_with_corner_bias_merson_region_segement_cost(gameState)
    return 0

    # Alternative heuristic function : Rejected
    # elif heuristic_number == 3:
    #     return heuristics.get_only_corner_bias_cost(gameState)
    # elif heuristic_number == 4:
    #     return heuristics.pagoda_with_isolation_count_as_well(gameState)
    # elif heuristic_number == 5:
    #     return heuristics.pagoda_with_partial_corner_and_no_merson_cost(gameState)

    # return heuristics.pagoda_1(gameState)