import readGame
import sys
import config
import pegSolitaireUtils
from copy import copy, deepcopy



###########################################
# Heuristic functions Used
###########################################

def get_manhattan_distance_cost(gameState): 
	""" This is manhattan distance cost matrix """

	euclidean_cost = 0

	for i in xrange(0,7):
		for j in xrange(0,7):
			if gameState[i][j] == 1:
				euclidean_cost += abs(3-i)+abs(3-j)

	return euclidean_cost

# [ 0, 0, 4, 0, 4, 0, 0 ]
# [ 0, 0, 0, 0, 0, 0, 0 ] 
# [ 4, 0, 3, 0, 3, 0, 4 ] 
# [ 0, 0, 0, 1, 0, 0, 0 ] 
# [ 4, 0, 3, 0, 3, 0, 4 ] 
# [ 0, 0, 0, 0, 0, 0, 0 ] 
# [ 0, 0, 4, 0, 4, 0, 0 ]
def pagoda_with_corner_bias_merson_region_segement_cost(gameState):
	""" This is typical pagoda with corner bias and merson region segment taken into account as well """

	costMatrix = [ [ 0, 0, 4, 0, 4, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0 ], [ 4, 0, 3, 0, 3, 0, 4 ], [ 0, 0, 0, 1, 0, 0, 0 ], [ 4, 0, 3, 0, 3, 0, 4 ], [ 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 4, 0, 4, 0, 0 ]]

	return evaluateCostMatrix(gameState, costMatrix)

###########################################
# Heuristic Functions Studied but not used
###########################################

def get_only_corner_bias_cost(gameState):
	""" cost matrix with only corners having cost associated. """

	edge_un_jumpable_pegs = [(0,2),(0,4),(2,0),(4,0),(6,2),(6,4),(4,6),(2,6)]
	heuristic_cost_of_corners = 0

	for i in xrange(0,7):
		for j in xrange(0,7):
			if gameState[i][j] == 1 and (i,j) in edge_un_jumpable_pegs:
				heuristic_cost_of_corners += 1

	return heuristic_cost_of_corners

def pagoda_with_isolation_count_as_well(gameState):
	""" modified pagoda (with corner bias and merson region cost) : 
	adding isolation cost. Seems to be performing bad """

	value_from_weightedCost = pagoda_with_corner_bias_merson_region_segement_cost(gameState)
	value_includes_isolation_count = count_isolated(gameState,value_from_weightedCost)

	return value_includes_isolation_count

# [ 0, 0, 1, 0, 1, 0, 0 ]
# [ 0, 0, 0, 0, 0, 0, 0 ] 
# [ 1, 0, 1, 0, 1, 0, 1 ] 
# [ 0, 0, 0, 0, 0, 0, 0 ] 
# [ 1, 0, 1, 0, 1, 0, 1 ] 
# [ 0, 0, 0, 0, 0, 0, 0 ] 
# [ 0, 0, 1, 0, 1, 0, 0 ]
def pagoda_with_partial_corner_and_no_merson_cost(gameState):
	""" Corner bias missing (Corners are weighted but not separated from the smaller mersion segments). 
	So performance of pagoda_with_corner_bias_merson_region_segement_cost is better. """

	pagoda_indices = [(0,2),(0,4),(2,0),(4,0),(6,2),(6,4),(4,6),(2,6),(2,2),(2,4),(4,2),(4,4)]
	pagoda_cost = 0

	for i in xrange(0,7):
		for j in xrange(0,7):
			if gameState[i][j] == 1 and (i,j) in pagoda_indices:
				pagoda_cost += 1

	return pagoda_cost

def get_game_cost_dummy(gameState):
	""" Just a dummy to test """
	
	return 0

###########################################
# Helper Functions
###########################################

def get_gameState_string(gameState):
    """ Convert gameState matrix into string """

    state_string=""
    for i in range(0,7):
        for j in range(0,7):
            state_string += str(gameState[i][j])
    return state_string

def count_isolated(gameState,value_from_pagoda_2):
	""" count the number of pegs in isolation (without any neighbour)"""
	
	isolated_peg_count = 0
	list_of_corner_pos = [(0,0),(0,1),(1,0),(1,1),(5,0),(6,0),(5,1),(6,1),(0,5),(0,6),(1,5),(1,6),(5,5),(5,6),(6,5),(6,6)]
	
	for i in xrange(0,7):
		for j in xrange(0,7):
			flag = False
			if i-1>=0 and (i-1,j) not in list_of_corner_pos and gameState[i-1][j] == 1:
				flag = True
			if j-1>=0 and (i,j-1) not in list_of_corner_pos and gameState[i][j-1] == 1:
				flag = True
			if i+1<=6 and (i+1,j) not in list_of_corner_pos and gameState[i+1][j] == 1:
				flag = True
			if j+1<=6 and (i,j+1) not in list_of_corner_pos and gameState[i][j+1] == 1:
				flag = True

			if flag == False:
				isolated_peg_count += 1
	return isolated_peg_count

def evaluateCostMatrix(gameState,costMatrix):
	""" evaluates weighted cost of gamestate according to the given cost matrix """
	
	cost = 0
	for i in xrange(0,7):
		for j in xrange(0,7):
			if gameState[i][j] == 1:
					cost += costMatrix[i][j]	
	return cost	

def get_symmetric_game_states(gameState):
	""" takes gameState and returns list of string_gameStates which are symmestric to given gameState """
	
	symmetric_game_states = []
	# check if the current state also needs to be added in the symmetric game states

	for i in xrange(0,3):
		rotate_game_state(gameState)
		symmetric_game_states.append(get_gameState_string(gameState))
	rotate_game_state(gameState)

	make_vertical_reflected_game_state(gameState)
	symmetric_game_states.append(get_gameState_string(gameState))
	
	for i in xrange(0,3):
		rotate_game_state(gameState)
		symmetric_game_states.append(get_gameState_string(gameState))
	rotate_game_state(gameState)
	make_vertical_reflected_game_state(gameState)

	return symmetric_game_states

def rotate_game_state(gameState):
	""" Rotates gamestate by 90 Degree """
	
	for i in xrange(0,7):
		for j in xrange(0,7):
			temp = gameState[i][j];
			gameState[i][j] = gameState[6-j][i];
			gameState[6-j][i] = gameState[6-i][6-j];
			gameState[6-i][6-j] = gameState[j][6-i];
			gameState[j][6-i] =temp;

	return gameState


def make_vertical_reflected_game_state(gameState):
	""" makes gameState, its vertical reflection """

	for j in xrange(0,3):
		for i in xrange(0,6):
			temp = gameState[i][j];
			gameState[i][j] = gameState[i][6-j];
			gameState[i][6-j] = temp;

	return gameState
