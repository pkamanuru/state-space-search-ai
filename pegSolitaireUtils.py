import readGame
import sys
import config

#######################################################
# These are some Helper functions which you have to use 
# and edit.
# Must try to find out usage of them, they can reduce
# your work by great deal.
#
# Functions to change:
# 1. is_wall(self, pos):
# 2. is_validMove(self, oldPos, direction):
# 3. getNextPosition(self, oldPos, direction):
# 4. getNextState(self, oldPos, direction):
#######################################################
class game:
    def __init__(self, filePath):
        self.gameState = readGame.readGameState(filePath)
        self.nodesExpanded = 0
        self.trace = [] 
        self.game_state_depth = 0
        self.previous_move = ((0,0),'')
        self.current_level_nodes_expanded=0

    def is_corner(self, pos):
        ########################################
        # You have to make changes from here
        # check for if the new positon is a corner or not
        # return true if the position is a corner 

        # The corner pegs here are 2X2 restricted pegs
        list_of_corner_pos = [(0,0),(0,1),(1,0),(1,1),(5,0),(6,0),(5,1),(6,1),(0,5),(0,6),(1,5),(1,6),(5,5),(5,6),(6,5),(6,6)]
        if pos in list_of_corner_pos:
            return True
        return False    


    def getNextPosition(self, oldPos, direction):
        #########################################
        # YOU HAVE TO MAKE CHANGES HERE
        # See DIRECTION dictionary in config.py and add
        # this to oldPos to get new position of the peg if moved
        # in given direction , you can remove next line
        (x,y) = oldPos
        (dx,dy) = config.DIRECTION[direction]
        x += 2*dx
        y += 2*dy
        #newPos = map(sum,zip(oldPos,config.DIRECTION[direction]))
        #newPos = map(sum,zip(newPos,config.DIRECTION[direction]))
        return (x,y)


    def is_validMove(self, oldPos, direction):
        #########################################
        # DONT change Things in here
        # In this we have got the next peg position and
        # below lines check for if the new move is a corner
        newPos = self.getNextPosition(oldPos, direction)
        if self.is_corner(newPos):
            return False    
        #########################################

        ########################################
        # YOU HAVE TO MAKE CHANGES BELOW THIS
        # check for cases like:
        # if new move is already occupied
        # or new move is outside peg Board
        # Remove next line according to your convenience
        if ((newPos[0]<0 or newPos[0]>6) or (newPos[1]<0 or newPos[1]>6) or (self.gameState[(newPos[0]+oldPos[0])/2][(newPos[1]+oldPos[1])/2] != 1) or self.gameState[newPos[0]][newPos[1]] == 1 ):
            return False
        return True

    def getNextState(self, oldPos, direction):
        ###############################################
        # DONT Change Things in here
        self.nodesExpanded += 1
        self.current_level_nodes_expanded += 1
        if not self.is_validMove(oldPos, direction):
            print "Error, You are not checking for valid move"
            exit(0)
        ###############################################

        ###############################################
        # YOU HAVE TO MAKE CHANGES BELOW THIS
        # Update the gameState after moving peg
        # eg: remove crossed over pegs by replacing it's
        # position in gameState by 0
        # and updating new peg position as 1
        newPos = self.getNextPosition(oldPos, direction)
        self.gameState[(newPos[0]+oldPos[0])/2][(newPos[1]+oldPos[1])/2] = 0
        self.gameState[oldPos[0]][oldPos[1]] = 0
        self.gameState[newPos[0]][newPos[1]] = 1
        return self.gameState

    def is_goalState(self):
        ###############################################
        # Check if the current gameState is the goal state
        for i in range(0,7):
            for j in range(0,7):
                if i!=3 or j!= 3:
                    if self.gameState[i][j] == 1:
                        return False
        if self.gameState[3][3]!= 1:
            return False

        return True 

    def get_gameState_string(self):
        state_string=""
        for i in range(0,7):
            for j in range(0,7):
                state_string += str(self.gameState[i][j])
        return state_string

    def has_peg(self,i,j):
        if (self.gameState[i][j] == 1):
            return True

    def print_solitare(self):
        for i in range(0,7):
            for j in range(0,7):
                if ( self.gameState[i][j] == -1 ):
                    sys.stdout.write(' '); 
                else:
                    sys.stdout.write(str(self.gameState[i][j]))
            sys.stdout.write('\n')

    def get_all_possible_game_moves(self):
        valid_game_moves = []
        available_directions = ['N','S','E','W']
        for i in range(0,7):
            for j in range(0,7):
                for direction in available_directions:
                    if not self.is_corner((i,j)) and self.has_peg(i,j) and self.is_validMove((i,j),direction):
                        #print "Adding %d and %d with Direction %c as valid", (i,j,direction)
                        valid_game_moves.append(((i,j),direction))
        return valid_game_moves

    def get_game_state(self):
        return self.gameState
        
    def set_game_state(self,game_state):
        self.gameState = [row[:] for row in game_state]
